//
// Created by luislenta on 22/4/20.
//
/**
 * Recibe como parametros la key de la cola
 */
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#define SIZE 1024
#define KEY_PATH "/tmp/mqueue_server_key"
#define USR_ERR "Usuario o contrase;a incorrecta"
#define USR_OK  "Bienvenido "
#define PROJECT_ID 'S'
#define PERM 0666
#define OK 10
#define ERR -1
#define AUTH_MSG 1

key_t key;
int32_t msgid;
// structure for message queue
struct mesg_buffer {
    long mesg_type;
    char mesg_buff[SIZE];
} message;

int main (/*int argc, char *argv[]*/)
{
  printf ("Proceso auth [%d] : \n", getpid ());
  key = ftok (KEY_PATH, PROJECT_ID);
  msgid = msgget (key, PERM | IPC_CREAT);

  message.mesg_type = AUTH_MSG;
  msgsnd(msgid, &message, sizeof(message), 0);

  // display the message
  printf("Message OK send");

  while(1){

    if(msgrcv (msgid,&message, sizeof (message),AUTH_MSG, 0) < 0)
        {
          perror ("Auth service error");
          exit (EXIT_FAILURE);
        }
        if(message.mesg_type == AUTH_MSG){
          if(strcmp ("luis;123",message.mesg_buff) == 0){
            message.mesg_type = AUTH_MSG;
            bzero (message.mesg_buff, sizeof (message));
            strcpy(message.mesg_buff, USR_OK);
          }
          else{
            message.mesg_type = AUTH_MSG;
            strcpy(message.mesg_buff, USR_ERR);
          }
            msgsnd (msgid,&message, sizeof (message),0);
        }
  }
  exit (EXIT_SUCCESS);
}