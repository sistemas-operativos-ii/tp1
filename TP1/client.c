#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#include <arpa/inet.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#define PARAMS 3
#define LIMIT 50
#define SIZE 1024

struct sockaddr_in serv_addr;
int32_t sockfd = 0;
char pass[LIMIT], user[LIMIT], buffer[SIZE];

void init (char *argv[]);
void auth ();
void welcomemsg ();

int main (int argc, char *argv[])
{

    //Only accept 3 parameters
    if (argc != PARAMS)
        {
            fprintf (stderr, "Formato incorrecto. Por favor, ingrese <IP> <PUERTO>");
            exit (EXIT_FAILURE);
        }

    init (argv);

    welcomemsg ();
        auth ();
    while(1){
        if (read (sockfd, buffer, sizeof (buffer)) < 0)
          {
            perror ("Error");
            exit (EXIT_FAILURE);
          }
          send (sockfd, "contexto", sizeof (buffer),0);
    }

    exit (EXIT_SUCCESS);
}

void init (char *argv[])
{

    //Set serv_addr
    if (inet_pton (AF_INET, argv[1], &serv_addr.sin_addr) < 1)
        {
            fprintf (stderr, "Dirección IP incorrecta. Se admite formato IPv4. Ej: 192.168.20.2");
            exit (EXIT_FAILURE);
        }
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons (atoi (argv[2]));

    //Create socket
    sockfd = socket (AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        {
            perror ("Error");
            exit (EXIT_FAILURE);
        }

    //Connect sockt
    if (connect (sockfd, (struct sockaddr *) &serv_addr, sizeof (serv_addr)) < 0)
        {
            perror ("Error");
            exit (EXIT_FAILURE);
        }
}

void auth ()
{
    //Init user and pass
    //bzero (user, sizeof (user));
    //bzero (pass, sizeof (pass));
    bzero (buffer, sizeof (buffer));  //TODO controlar si hay que borrar

    printf ("Por favor, ingrese su usuario y presione enter.\n");
    fgets (user, LIMIT, stdin);

    printf ("Por favor, ingrese su contraseña y presione enter.\n");
    fgets (pass, LIMIT, stdin);

    //Make message
    strcat (pass, "\0");
    strcpy (buffer, strtok (user, "\n"));
    strcat (buffer, ";");
    strcat (buffer, strtok (pass, "\n"));


    if (write (sockfd, buffer, sizeof (buffer)) < 0)
        {
            perror ("Error");
            exit (EXIT_FAILURE);
        }

   // bzero (buffer, sizeof (buffer)); //TODO controlar si hay que borrar



}

void welcomemsg ()
{
    printf ("\n\n\n------------------------------------------\n");
    printf ("|                 Bienvenido              |\n");
    printf ("|  Autentíquese para acceder al servidor  |\n");
    printf ("------------------------------------------\n");
}

