//
// Created by luislenta on 11/4/20.
//
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/msg.h>

//TODO Cambiar elemento separador ; por otro

#define KEY_PATH "/tmp/mqueue_server_key"
#define PROJECT_ID 'S'
#define PORT 8080
#define CONNECTIONS 5
#define SIZE 1024
#define PERM 0666
#define ERR -1
#define AUTH_MSG 1
#define LS_MSG 2
#define PASS_MSG 3
#define OK 10
#define FILE_OK 1
#define USER "user"
#define FILE "file"

socklen_t clilen;
int32_t serverfd, newsockfd, msgid;
key_t key;
//pid_t  authid, fsid;
struct sockaddr_in serv_addr, cli_addr;
char buffer[SIZE];

// structure for message queue
struct mesg_buffer {
    long mesg_type;
    char mesg_buff[SIZE];
} message;

void socket_init ();
void auth_init ();
void fs_init ();
void welcomemsg ();

int main ()
{
  //Init MSGQUEUE
  key = ftok (KEY_PATH, PROJECT_ID);
  msgid = msgget (key, PERM | IPC_CREAT);


  //fs_init ();
  //Init child process

  //pid_t authid = fork ();
  int authid = 1;
  if (authid < 0)
    {
      printf ("Create auth child error");
      exit (EXIT_FAILURE);
    }
  else if (authid == 0)
    {
      auth_init ();
    }
  else
    {
      socket_init ();
      int firsttime = 0;
      while (1)
        {
          //bzero (buffer, sizeof (buffer)); //TODO Borrar, no es necesario estar limpiando el buffer

          if (read (newsockfd, buffer, sizeof (buffer)) < 0)
            {
              perror ("Read Error");
              exit (EXIT_FAILURE);
            }

          if (firsttime == 0)
            {
              firsttime++;
              //Check if authservice is up.
              if (msgrcv (msgid, &message, sizeof (message), AUTH_MSG, 0) < 0)
                {
                  perror ("Receive auth message error");
                  exit (EXIT_FAILURE);
                }

              printf ("MSG OKl");

              message.mesg_type = AUTH_MSG;
              strcat (message.mesg_buff, strtok (buffer, "\0")); //TODO controlar si es necesario checkear char null
              msgsnd (msgid, &message, sizeof (message), 0);

              if (msgrcv (msgid, &message, sizeof (message), AUTH_MSG, 0) < 0)
                {
                  perror (message.mesg_buff);
                  exit (EXIT_FAILURE);
                }
              printf ("MSG22222 OKl");

              write (newsockfd, message.mesg_buff, sizeof (message));
            }
          else
            {

              write (newsockfd, "segunda", sizeof (buffer));
              if (strcmp (strtok (buffer, ";"), USER))
                {

                }
              if (strcmp (strtok (buffer, ";"), FILE))
                {

                }
            }

        }
    }
  close (newsockfd);
  close (serverfd);
}

void socket_init ()
{

  serverfd = socket (AF_INET, SOCK_STREAM, 0);
  if (serverfd < 0)
    {
      perror ("Socket error");
      exit (EXIT_FAILURE);
    }

  //Set serv_addr
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;    /* Address to accept any incoming messages.  */
  serv_addr.sin_port = htons (PORT);

  if (bind (serverfd, (struct sockaddr *) &serv_addr, sizeof (serv_addr)) < 0)
    {
      printf ("%d", getpid ());
      perror ("Bind error ");
      exit (EXIT_FAILURE);
    }

  if (listen (serverfd, CONNECTIONS) < 0)
    {
      perror ("Listen error");
      exit (EXIT_FAILURE);
    }

  clilen = sizeof (cli_addr);
  newsockfd = accept (serverfd, (struct sockaddr *) &cli_addr, &clilen);
  //TODO Checkear si es necesario controlar el error del accept
  /*if (newsockfd < 1)
    {
      perror ("Accept Error");
      exit (EXIT_FAILURE);
    }*/
}
void auth_init ()
{

  char *argv_list[] = {NULL};
  if (execv ("./authservice", argv_list) < 0)
    {
      perror ("Execution auth service error");
      exit (EXIT_FAILURE);
    }
  exit (EXIT_SUCCESS);
}
/*
void fs_init ()
{
  fsid = fork ();

  if (fsid < 0)
    {
      printf ("Create auth child error");
      exit (EXIT_FAILURE);
    }
  else if (fsid == 0)
    {

    }
  else
    {
      return;
    }
}*/
void welcomemsg ()
{
  printf ("\n\n\n------------------------------------------\n");
  printf ("|                       Bienvenido                   |\n");
  printf ("|   El servidor esta listo para recibir sus comandos |\n");
  printf ("------------------------------------------\n");
}